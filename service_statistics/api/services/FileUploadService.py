from configparser import ConfigParser

from openpyxl import load_workbook
import requests
from werkzeug.datastructures import FileStorage
import json
from service_statistics.api.JSONEncoder import JSONEncoder
from service_statistics.api.services import FileUploadFactory


class FileUploadService(object):
    def __init__(self):
        config = ConfigParser()
        config.read('keyConfigs.conf')
        self.config = config['ServiceUrls']
        self.jsonEncoder = JSONEncoder()

    def process(self, fileStream: FileStorage):

        file_reader = FileUploadFactory.getFileReader(fileStream.filename)

        wb = load_workbook(filename=fileStream.stream)

        try:
            player_list = file_reader.load_players(wb)
            payload = {"players": [p.getProperties() for p in player_list]}
            headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

            url = self.config['PlayersServiceUrl'] + self.config['PlayersUploadEndpoint']
            response = requests.put(url, json=payload, headers=headers)

            if response.status_code != 201:
                raise SystemError

            committed_ids = json.loads(response.text)

            for p, id in zip(player_list, committed_ids):
                p.set_id(id)

        except Exception as err:
            print(err)
            raise err


        # fixture_list = file_reader.load_fixtures(wb)
        #
        # player_attributes_list = file_reader.load_player_attributes(wb)

        # TODO: Implement HttpClient to send player collection to the database
