from service_statistics.api.models.Season import Season

class SeasonsService(object):
    def __init__(self, db):
        self.dsManager = db

    def find(self, id: str) -> Season:
        return self.dsManager.findSeasonById(id)

    def findAll(self) -> list:
        return self.dsManager.findAllSeasons()

    def add(self, data) -> Season:
        if not isinstance(data, Season):
            try:
                season = self.__generateSeason(data)
            except Exception:
                raise AttributeError('Cannot generate season instance')
        else:
            season = data

        return self.dsManager.addSeason(season)

    def addMany(self, data: []) -> list:
        return [self.add(s) for s in data]

    def update(self, data: {}) -> bool:
        if not isinstance(data, Season):
            try:
                season = self.__generateSeason(data)
            except Exception:
                raise AttributeError('Cannot generate season instance')
        else:
            season = data

        return self.dsManager.updateSeason(season)

    def remove(self, id: str) -> bool:
        return self.dsManager.removeSeason(id)

    def __generateSeason(self, data: {}) -> Season:
        season = Season(str(data['year']), str(data['initial_day']), data['fixtures'])

        if '_id' in data:
            season.set_id(data['_id'])

        return season