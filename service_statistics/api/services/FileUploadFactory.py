from service_statistics.api.processors.XLSXFileReader import XLSXFileReader


def getFileReader(fileName):
    fileData = fileName.split('.')

    if not fileData:
        return None

    file_uploader_options = {
        'xlsx': XLSXFileReader(fileData[0]),
        'csv': None
    }

    return file_uploader_options.get(fileData[1], XLSXFileReader(fileData[0]))