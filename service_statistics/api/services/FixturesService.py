from service_statistics.api.data_services import FixturesDataServiceManager
from service_statistics.api.models.Fixture import Fixture


class FixturesService(object):
    def __init__(self):
        self.dsManager = FixturesDataServiceManager.FixturesDataServiceManager()

    def find(self, id: str) -> Fixture:
        return self.dsManager.findFixtureById(id)

    def findAll(self) -> list:
        return self.dsManager.findAllFixtures()

    def add(self, data) -> str:
        if not isinstance(data, Fixture):
            try:
                fixture = self.__generateFixture(data)
            except Exception:
                raise AttributeError('Cannot generate Fixture instance')
        else:
            fixture = data

        return self.dsManager.addFixture(fixture)

    def addMany(self, data: []) -> list:
        return [self.add(s) for s in data]

    def update(self, data: {}) -> bool:
        if not isinstance(data, Fixture):
            try:
                fixture = self.__generateFixture(data)
            except Exception:
                raise AttributeError('Cannot generate Fixture instance')
        else:
            fixture = data

        return self.dsManager.updateFixture(fixture)

    def remove(self, id: str) -> bool:
        return self.dsManager.removeFixture(id)

    def __generateFixture(self, data: {}) -> Fixture:
        fixture = Fixture(data['goals'], data['assists'], data['teams'], data['game_status'], data['fixture_date'])

        if '_id' in data:
            fixture.set_id(data['_id'])

        return fixture