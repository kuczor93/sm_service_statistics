from configparser import ConfigParser

import holidays
from openpyxl import Workbook

from service_statistics.api.models.Fixture import Fixture
from service_statistics.api.models.Player import Player
from service_statistics.api.models.PlayerActivity import PlayerActivity
from service_statistics.api.models.PlayerAttribute import PlayerAttribute
from service_statistics.api.models.Season import Season


class XLSXFileReader(object):

    def __init__(self, fileName: str):

        self.__CONFIG_SECTION = 'SpreadsheetVariables'
        self.load_config()
        data = fileName.lower().split('_')

        if not data:
            raise AttributeError('No Data specified')

        if data[1] == 'monday':
            self.game_time_slot = 'MON'
        elif data[1] == 'thursday':
            self.game_time_slot = 'THU'
        else:
            raise KeyError('Event date not supported yet')

        self.season_year = int(list(filter(str.isdigit, data[0]))[0])
        self._players = []
        self._fixtures = []
        self._season = None
        self._player_attributes = []

    def load_config(self):
        self.config = ConfigParser()
        self.config.read('keyConfigs.conf')

    def find_player_start_index(self, work_sheet, keyword: str) -> int:
        is_found = False
        index = 1

        while not is_found:
            cell = work_sheet['{}{}'.format(self.config[self.__CONFIG_SECTION]['PlayersCol'], index)]
            if cell.value and cell.value.lower() == keyword.lower():
                # NOTE: Index is incremented by 1 to skip scanning of keyword cell
                return index + 1
            index += 1
        else:
            raise IndexError('Can\'t find a valid Player keyword')

    def load_players(self, wb: Workbook) -> list:
        keyword = 'player'
        is_finished = False

        ws = wb.worksheets[
            wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['PlayersTab'].format(self.game_time_slot))]

        counter = self.find_player_start_index(ws, keyword)

        while not is_finished:
            cellValue = ws['{}{}'.format(self.config[self.__CONFIG_SECTION]['PlayersCol'], counter)]
            if cellValue.value:
                player_name = cellValue.value.split(' ')

                if len(player_name) == 1:
                    player_name.append('')

                playerData = Player(firstName=player_name[0], lastName=player_name[1], preferredFoot='',
                                    nickName='', yearOfBirth=0)
                playerData.set_row(int(''.join(list(filter(str.isdigit, cellValue.coordinate)))))
                self._players.append(playerData)
            else:
                is_finished = True
            counter += 1

        return self._players

    def load_fixtures(self, wb: Workbook):

        if not self._fixtures:
            self.create_default_fixtures(wb)

        self.load_fixture_goals(wb)
        self.load_fixture_assists(wb)
        self.load_fixture_teams(wb)

        return self._fixtures

    def create_default_fixtures(self, wb: Workbook):
        dateRowIndex = 1
        dateRowFound = False
        ws = wb.worksheets[
            wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['FixturesTab'].format(self.game_time_slot))]

        start_col = self.config[self.__CONFIG_SECTION]['FixturesColStart']

        while not dateRowFound:
            if ws['{}{}'.format(start_col, dateRowIndex)].is_date:
                dateRowFound = True
                break
            dateRowIndex += 1

        local_holidays = holidays.UK()

        for cell in tuple(ws.rows)[dateRowIndex - 1]:
            if cell.value and cell.is_date:
                f = Fixture({},{}, [], '3', str(cell.value))
                f.set_col(''.join(list(filter(str.isalpha, cell.coordinate))))
                f.is_bank_holiday(f.get_fixture_date() in local_holidays)

                self._fixtures.append(f)

    def load_fixture_goals(self, wb: Workbook):
        ws = wb.worksheets[
            wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['GoalsTab'].format(self.game_time_slot))]

        start_index = self.find_player_start_index(ws, 'player')

        # NOTE: start_index points to first record skipping keyword. To calculate last one,
        # we need to do opposite and to one record below
        final_index = len(self._players) + (start_index - 1)

        for fixture in self._fixtures:
            f_cells = ws['{}{}'.format(fixture.get_col(), start_index) : '{}{}'.format(fixture.get_col(), final_index)]
            for cell in f_cells:
                cell = cell[0]
                if cell.value and int(cell.value) > 0:
                    p_key = str([p.get_id() for p in self._players if p.get_row() == cell.row][0]).strip()
                    fixture.add_goalscorer(p_key, cell.value)


    def load_fixture_assists(self, wb: Workbook):
        ws = wb.worksheets[
            wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['AssistsTab'].format(self.game_time_slot))]

        start_index = self.find_player_start_index(ws, 'player')

        # NOTE: start_index points to first record skipping keyword. To calculate last one,
        # we need to do opposite and to one record below
        final_index = len(self._players) + (start_index - 1)

        for fixture in self._fixtures:
            f_cells = ws['{}{}'.format(fixture.get_col(), start_index) : '{}{}'.format(fixture.get_col(), final_index)]
            for cell in f_cells:
                cell = cell[0]
                if cell.value and int(cell.value) > 0:
                    p_key = str([p.get_id() for p in self._players if p.get_row() == cell.row][0]).strip()
                    fixture.add_assist(p_key, cell.value)

    def load_fixture_teams(self, wb: Workbook):
        ws = wb.worksheets[wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['TeamsTab'])]
        is_complete = False
        range_counter = {
            'date': None,
            'start': 0,
            'end': 0
        }

        record_count = len(ws['{}'.format(self.config[self.__CONFIG_SECTION]['DateCol'])])

        index = 2 # NOTE: Starting from second row to avoid indexed values
        while not is_complete:
            cell = ws['{}{}'.format(self.config[self.__CONFIG_SECTION]['DateCol'], index)]

            if cell.is_date or record_count == index:
                if range_counter['date'] is not None:
                    range_counter['end'] = index - 1

                    fixture = [f for f in self._fixtures if f.get_fixture_date() == range_counter['date']][0]

                    team_added = 0
                    for fixture_index, key in enumerate(['WinCol', 'LoseCol', 'DrawCol']):
                        data_cells = ws['{}{}'.format(
                            self.config[self.__CONFIG_SECTION][key], range_counter['start']) : '{}{}'.format(
                            self.config[self.__CONFIG_SECTION][key], range_counter['end'])]

                        if team_added < 2:
                            if data_cells and fixture_index == 2:
                                if all([c[0].value for c in data_cells]):
                                    i = int(len(data_cells)/2)
                                    team1 = {
                                        'captain': '',
                                        'players': [self.get_player_id_by_name(c[0].value.lower())
                                                    for c in data_cells][:i],
                                        'fixture_game_status': fixture_index
                                    }
                                    team2 = {
                                        'captain': '',
                                        'players': [self.get_player_id_by_name(c[0].value.lower())
                                                    for c in data_cells][i:],
                                        'fixture_game_status': fixture_index
                                    }
                                    fixture.add_team(team1)
                                    fixture.add_team(team2)
                                    team_added += 2
                            else:
                                team_cells = [c[0].value for c in data_cells]

                                if all(team_cells):
                                    team_added += 1
                                    team = {
                                        'captain': '',
                                        'players': [self.get_player_id_by_name(tc.lower()) for tc in team_cells],
                                        'fixture_game_status': fixture_index
                                    }
                                    fixture.add_team(team)

                if record_count == index:
                    is_complete = True

                range_counter['date'] = cell.value
                range_counter['start'] = index
            index += 1

    def generate_season(self):

        fixtures = [str(f) for f in self._fixtures]

        self._season = Season(str(self.season_year), fixtures)

        return self._season

    def load_player_attributes(self, wb: Workbook):
        ws = wb.worksheets[wb.sheetnames.index(self.config[self.__CONFIG_SECTION]['AttributesTab'])]
        colStart = self.config[self.__CONFIG_SECTION]['AttributesColStart']
        rowStart = self.config[self.__CONFIG_SECTION]['AttributesRowStart']
        colEnd = self.config[self.__CONFIG_SECTION]['AttributesColEnd']
        is_finished = False

        index = int(rowStart)
        while not is_finished:
            data_cells = ws[f'{colStart}{index}' : f'{colEnd}{index}']
            converted_cells = [dc.value for dc in data_cells[0]]

            if not converted_cells[0]:
                is_finished = True

            if any(converted_cells[1:]):
                p_activity = PlayerActivity(inactive=converted_cells[4], jog=converted_cells[5],
                                            run=converted_cells[6], hi_speed=converted_cells[7],
                                            sprint=converted_cells[8])
                p_attribute = PlayerAttribute('', max_speed=converted_cells[1], distance=converted_cells[2],
                                              sprints=converted_cells[3], activity=p_activity)
                p_attribute.set_internal_player(str(converted_cells[0]).lower())

                p_attribute.set_id(self.get_player_id_by_name(str(converted_cells[0]).lower()))

                self._player_attributes.append(p_attribute)

            index += 1

        return self._player_attributes

    def get_player_id_by_name(self, name: str):
        p_ids = [p.get_id() for p in self._players if p.get_name() == name]

        if p_ids and len(p_ids) == 1:
            return p_ids[0]
        return ''