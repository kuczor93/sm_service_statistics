from werkzeug.datastructures import FileStorage

from service_statistics.api.restplus import api
from flask_restplus import fields

seasonModel = api.model('Season', {
    '_id': fields.String(description='The unique identifier for each season'),
    'year': fields.String(description='Calendar year of the season', required=True),
    'initial_day': fields.String(description='First calendar date of the season', required=True),
    'fixtures': fields.List(fields.String, description='List of fixture id\'s within a specific season')
})

seasonModels = api.inherit('Collection of Season Instances', {
    'seasons': fields.List(fields.Nested(seasonModel))
})

teamModel = api.model('model', {
    'captain': fields.String(description='player Id representing a captain in a given fixture', required=True),
    'players': fields.List(fields.String, description='List of participating players for a given team', required=True)
})

fixtureModel = api.model('Fixture', {
    '_id': fields.String(description='The unique identifier for each fixture'),
    'goals': fields.List(fields.String, description='List of goalscores for the fixture', required=True),
    'assists': fields.List(fields.String, description='List of goal assistants for the fixture', required=True),
    'teams': fields.List(fields.Nested(teamModel), required=True),
    'fixture_date': fields.DateTime(description='Date describing when the fixture has happened', required=True)
})

upload_parser = api.parser()
upload_parser.add_argument('file', location='files', type=FileStorage, required=True)