class PlayerActivity(object):

    def __init__(self, inactive, jog, run, hi_speed, sprint):
        self.__inactive = self.convert_value(inactive)
        self.__jog = self.convert_value(jog)
        self.__run = self.convert_value(run)
        self.__hi_speed = self.convert_value(hi_speed)
        self.__sprint = self.convert_value(sprint)

    def set_inactive(self, value: object):
        self.__inactive = self.convert_value(value)

    def set_jog(self, value: object):
        self.__jog = self.convert_value(value)

    def set_run(self, value: object):
        self.__run = self.convert_value(value)

    def set_hi_speed(self, value: object):
        self.__hi_speed = self.convert_value(value)

    def set_sprint(self, value: object):
        self.__sprint = self.convert_value(value)

    def convert_value(self, value: object) -> object:
        if isinstance(value, str):
            return float(value)
        elif isinstance(value, int):
            return float(value)
        elif isinstance(value, float):
            return value
        else:
            raise ValueError('Cannot convert value to float')

    def get_properties(self):

        result = {
            'inactive': self.__inactive,
            'jog': self.__jog,
            'run': self.__run,
            'hi_speed': self.__hi_speed,
            'sprint': self.__sprint
        }

        return result