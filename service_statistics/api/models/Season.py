class Season(object):
    """
        Season object is a collection of properties describing an annual collection of game statistics
    """

    def __init__(self, year: str, fixtures: list = None, id: str = None):
        """

        :param year:
        :param initial_day:
        :param fixtures:
        :param id (optional):
        """
        if fixtures is None:
            self.__fixtures = []
        else:
            self.__fixtures = fixtures
        self.__year = int(year)
        self.__id = id

    def __repr__(self):
        return '<Season {}>'.format(self.__year)

    def getProperties(self):

        properties = {
            'year': str(self.__year),
            'fixtures': self.__fixtures
        }
        if self.__id:
            properties['_id'] = self.__id

        return properties

    def set_id(self, id):
        self.__id = id
