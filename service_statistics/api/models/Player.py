class Player(object):
    """
        Player object is a collection of personal properties describing a football player
    """

    def __init__(self, firstName: str, lastName: str, preferredFoot: str, nickName: str = None,
                 yearOfBirth: int = None, id: str = None):
        """

        :param firstName:
        :param lastName:
        :param preferredFoot:
        :param nickName:
        :param yearOfBirth:
        """
        self.__id = id
        self.__firstName = self._to_camel_case(firstName)
        self.__lastName = self._to_camel_case(lastName)
        self.__preferredFoot = preferredFoot
        self.__nickName = self._to_camel_case(nickName)
        self.__yearOfBirth = yearOfBirth
        self.__worksheet_row = 0

    def __repr__(self):
        title = '{0}_{1}'.format(self.__firstName, self.__lastName)
        return '<Player %r>' % title

    def getProperties(self):
        properties = {
            "firstName": str(self.__firstName),
            "lastName": str(self.__lastName),
            "preferredFoot": str(self.__preferredFoot),
            "nickName": str(self.__nickName),
            "yearOfBirth": self.__yearOfBirth
        }

        if self.__id:
            properties["_id"] = str(self.__id)

        return properties

    def get_name(self):
        return '{0} {1}'.format(self.__firstName, self.__lastName)

    def get_row(self) -> int:
        return self.__worksheet_row

    def set_row(self, val: int):
        self.__worksheet_row = val

    def set_id(self, id):
        self.__id = id

    def get_id(self):
        return self.__id

    def _to_camel_case(self, value: str) -> str:
        if value:
            return value[0].upper() + value[1:].lower()
        return value