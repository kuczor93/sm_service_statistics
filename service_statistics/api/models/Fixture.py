from datetime import datetime

class Fixture(object):
    """
        Fixture object is a collection of properties describing a game fixture and its associated statistics
    """

    def __init__(self, goals: dict, assists: dict, teams: list, fixture_date: str,
                 id: str = None):
        """

        :param goals:
        :param assists:
        :param teams:
        :param fixture_date:
        :param id:
        """
        self.__goals = goals
        self.__assists = assists
        self.__teams = teams
        self.__worksheet_col = ''
        self.__bank_hol = False

        # TODO: handle exception in case date transform fails
        self.__fixture_date = datetime.strptime(fixture_date, "%Y-%m-%dT%H:%M:%S.%fZ")
        self.__id = id

        # VALIDATION
        self.validate_teams()

    def __str__(self):
        formatted_date = self.__fixture_date.strftime("%d-%m-%Y")
        return f'<Fixture {formatted_date}>'

    def __repr__(self):
        formatted_date = self.__fixture_date.strftime("%d-%m-%Y")
        return formatted_date

    def get_col(self) -> str:
        return self.__worksheet_col

    def set_col(self, val: str):
        self.__worksheet_col = val

    def getProperties(self):
        properties = {
            'goals': self.__goals,
            'assists': self.__assists,
            'teams': self.__teams,
            'fixture_date': self.__fixture_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        }
        if self.__id:
            properties['_id'] = self.__id

        return properties

    def set_id(self, id):
        self.__id = id

    def add_goalscorer(self, id: str, value: int):
        if id in self.__goals:
            raise KeyError('Key \'{}\' already exist'.format(id))

        self.__goals[id] = value

    def add_assist(self, id: str, value: int):
        if id in self.__assists:
            raise KeyError('Key \'{}\' already exist'.format(id))

        self.__assists[id] = value

    def add_team(self, team: object):
        self.__teams.append(team)

    def get_fixture_date(self) -> datetime:
        return self.__fixture_date

    def is_bank_holiday(self, value: bool):
        self.__bank_hol = value

    def validate_teams(self):
        for index, team in enumerate(self.__teams):
            if not 'captain' in team:
                raise ValueError('Team {} does not have assigned captain'.format(index))

            if not 'players' in team:
                raise ValueError('Team {} does not contain list of players'.format(index))
