from service_statistics.api.models.MetricsSystem import MetricsSystem
from service_statistics.api.models.PlayerActivity import PlayerActivity


class PlayerAttribute(object):
    """
        Player attribute object is a collection of attributes describing a football player
    """

    def __init__(self, player_Id, max_speed, distance, sprints, activity: PlayerActivity,
                 id: str = None, metrics_system: MetricsSystem = MetricsSystem.UK):

        self.__id = id
        self.__playerId = player_Id
        self.__maxSpeed = max_speed
        self.__distance = distance
        self.__sprints = sprints
        self.__activity = activity

        self.__internal_name = ''

        self.__metricsSystem = metrics_system

    def set_internal_player(self, value: str):
        self.__internal_name = value

    def set_id(self, value):
        self.__id = value

    def get_properties(self, dbInsert: bool = True):
        result = {
            'metrics_system': self.__metricsSystem,
            'max_speed': self.__maxSpeed,
            'distance': self.__distance,
            'sprints': self.__sprints,
            'activity': self.__activity.get_properties()
        }

        if dbInsert:
            result['player_id'] = self.__internal_name
        else:
            result['player_id'] = self.__playerId

        return result