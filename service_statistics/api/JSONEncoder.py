import json
from bson import ObjectId

from service_statistics.api.models.Fixture import Fixture
from service_statistics.api.models.Player import Player
from service_statistics.api.models.Season import Season


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, Season):
            return o.getProperties()
        elif isinstance(o, Player):
            return o.getProperties()
        elif isinstance(o, Fixture):
            return o.getProperties()
        return json.JSONEncoder.default(self, o)
