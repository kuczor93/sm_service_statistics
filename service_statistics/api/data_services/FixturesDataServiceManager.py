from bson import ObjectId

import service_statistics.app
from service_statistics.api.models.Fixture import Fixture
from service_statistics.settings import MONGODB_SCHEMAS

DB_SCHEMA = 'fixtures'

class FixturesDataServiceManager(object):

    def __init__(self):
        pass

    def findFixtureById(self, id) -> Fixture:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]
        output = None

        response = collection.find_one({'_id': ObjectId(id)})

        if response:
            output = Fixture(response['goals'], response['assists'], response['teams'],
                             response['fixture_date'], response['_id'])

        return output

    def findAllFixtures(self) -> list:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]
        output = []

        for record in collection.find():
            output.append(Fixture(record['goals'], record['assists'], record['teams'],
                                  record['fixture_date'], record['_id']))
        return output

    def addFixture(self, fixture: Fixture) -> str:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]

        modelHashMap = fixture.getProperties()
        fixtureId = collection.insert_one(modelHashMap).inserted_id

        return str(fixtureId)

    def updateFixture(self, fixture: Fixture) -> bool:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]

        fixture_json = fixture.getProperties()

        # NOTE: _id is immutable and needs to be removed from properties before update
        p_id = fixture_json.pop('_id', None)

        response = collection.update_one(
            {
                '_id': ObjectId(p_id)
            },
            {
                '$set': fixture_json
            })

        if response:
            return True

        return False

    def removeFixture(self, id: str) -> bool:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]

        response = collection.delete_one({'_id': ObjectId(id)})

        if response and response.deleted_count == 1:
            return True

        return False