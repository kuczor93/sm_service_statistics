from bson import ObjectId

import service_statistics.app
from service_statistics.api.models.Season import Season
from service_statistics.settings import MONGODB_SCHEMAS

DB_SCHEMA = 'seasons'


class SeasonsDataServiceManager(object):

    def __init__(self):
        pass

    def findSeasonById(self, id) -> Season:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]
        output = None

        response = collection.find_one({'_id': ObjectId(id)})

        if response:
            output = Season(response['year'], response['fixtures'], response['_id'])

        return output

    def findAllSeasons(self) -> list:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]
        output = []

        for record in collection.find():
            output.append(Season(record['year'], record['fixtures'],
                                 record['_id'].getProperties()))
        return output

    def addSeason(self, season: Season) -> Season:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]
        output = None

        modelHashMap = season.getProperties()
        seasonId = collection.insert_one(modelHashMap).inserted_id
        response = collection.find_one({'_id': seasonId})

        if response:
            output = Season(response['year'], response['fixtures'], response['_id'])
        return output

    def updateSeason(self, season: Season) -> bool:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]

        season_json = season.getProperties()

        # NOTE: _id is immutable and needs to be removed from properties before update
        p_id = season_json.pop('_id', None)

        response = collection.update_one(
            {
                '_id': ObjectId(p_id)
            },
            {
                '$set': season_json
            })

        if response:
            return True

        return False

    def removeSeason (self, id: str) -> bool:
        collection = service_statistics.app.db.db[MONGODB_SCHEMAS[DB_SCHEMA]]

        response = collection.delete_one({'_id': ObjectId(id)})

        if response and response.deleted_count == 1:
            return True

        return False