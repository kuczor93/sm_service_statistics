import logging

from flask import Response, request
from flask_restplus import Resource

from service_statistics.api.JSONEncoder import JSONEncoder
from service_statistics.api.model_serializers import fixtureModel
from service_statistics.api.restplus import api
from service_statistics.api.services.FixturesService import FixturesService

log = logging.getLogger(__name__)
ns = api.namespace('fixtures', description='Controller defining CRUD operations on fixtures Repository')

@ns.route('/')
class FixtureCollection(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jsonEncoder = JSONEncoder()
        self.fixturesService = FixturesService()

    @api.response(200, 'Returns list of all available fixtures')
    @api.response(204, 'No fixtures available in the repository')
    def get(self):
        """
        Returns list of all fixtures available in the database
        :return: List<Fixture>
        """
        try:
            response = self.fixturesService.findAll()

            if response:
                return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

    @api.expect(fixtureModel)
    @api.response(201, 'New fixture instance successfully created')
    @api.response(400, 'Bad data request')
    def post(self):
        """
        Creates and inserts fixture instance into the database
        :return: Fixture
        """

        if not request.json:
            return Response('No data provided', mimetype='application/json', status=400)

        try:
            response = self.fixturesService.add(request.json)

            if response:
                return Response(self.jsonEncoder.encode(response),
                                mimetype='application/json', status=201)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

@ns.route('/<string:id>')
class FixtureItem(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jsonEncoder = JSONEncoder()
        self.fixturesService = FixturesService()

    @api.response(200, 'Returns game season instance using provided season ID')
    @api.response(204, 'Season with provided Id does not exist in the repository')
    def get(self, id: str):
        """
        Returns game season instance
        :return: Season
        """

        response = self.fixturesService.find(id)

        if response:
            return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
        return Response(None, mimetype='application/json', status=404)

    @api.expect(fixtureModel)
    def put(self, id: str):
        """
        Updates season instance based on provided season Id
        :param id: unique season id
        :return: StatusCode
        """
        if '_id' not in request.json:
            return Response('Missing _id for the season', mimetype='application/json', status=400)

        response = self.fixturesService.update(request.json)

        if response:
            return Response(None, mimetype='application/json', status=200)

        return Response('Issue with a fixture update', mimetype='application/json', status=500)

    def delete(self, id: str):
        """
        Deletes player instance based on provided player id
        :param id: unique player id
        :return: StatusCode
        """
        deleted = self.fixturesService.remove(id)

        if deleted:
            return Response(None, mimetype='application/json', status=204)
        return Response('Issue with a fixture delete', mimetype='application/json', status=500)