import logging

from flask import Response, request
from flask_restplus import Resource

from service_statistics.api.JSONEncoder import JSONEncoder
from service_statistics.api.data_services import SeasonsDataServiceManager
from service_statistics.api.model_serializers import seasonModel
from service_statistics.api.restplus import api
from service_statistics.api.services.SeasonsService import SeasonsService

log = logging.getLogger(__name__)
ns = api.namespace('seasons', description='Controller defining CRUD operations on Seasons Repository')

@ns.route('/')
class SeasonCollection(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jsonEncoder = JSONEncoder()
        db = SeasonsDataServiceManager.SeasonsDataServiceManager()
        self.seasonsService = SeasonsService(db)

    @api.response(200, 'Returns list of all available seasons')
    @api.response(204, 'No seasons available in the repository')
    def get(self):
        """
        Returns list of all seasons available in the database
        :return: List<Season>
        """
        try:
            response = self.seasonsService.findAll()

            if response:
                return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

    @api.expect(seasonModel)
    @api.response(201, 'New season instance successfully created')
    @api.response(400, 'Bad data request')
    def post(self):
        """
        Creates and inserts season instance into the database
        :return: Player
        """

        if not request.json:
            return Response('No data provided', mimetype='application/json', status=400)

        try:
            response = self.seasonsService.add(request.json)

            if response:
                return Response(self.jsonEncoder.encode(response.getProperties()),
                                mimetype='application/json', status=201)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

@ns.route('/<string:id>')
class SeasonItem(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jsonEncoder = JSONEncoder()
        db = SeasonsDataServiceManager.SeasonsDataServiceManager()
        self.seasonsService = SeasonsService(db)

    @api.response(200, 'Returns game season instance using provided season ID')
    @api.response(204, 'Season with provided Id does not exist in the repository')
    def get(self, id: str):
        """
        Returns game season instance
        :return: Season
        """

        response = self.seasonsService.find(id)

        if response:
            return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
        return Response(None, mimetype='application/json', status=404)

    @api.expect(seasonModel)
    def put(self, id: str):
        """
        Updates season instance based on provided season Id
        :param id: unique season id
        :return: StatusCode
        """
        if '_id' not in request.json:
            return Response('Missing _id for the season', mimetype='application/json', status=400)

        response = self.seasonsService.update(request.json)

        if response:
            return Response(None, mimetype='application/json', status=200)

        return Response('Issue with a season update', mimetype='application/json', status=500)

    def delete(self, id: str):
        """
        Deletes player instance based on provided player id
        :param id: unique player id
        :return: StatusCode
        """
        deleted = self.seasonsService.remove(id)

        if deleted:
            return Response(None, mimetype='application/json', status=204)
        return Response('Issue with a season delete', mimetype='application/json', status=500)