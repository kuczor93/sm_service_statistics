import logging

from flask import request, Response
from flask_restplus import Resource

from service_statistics.api.JSONEncoder import JSONEncoder
from service_statistics.api.model_serializers import upload_parser
from service_statistics.api.restplus import api
from service_statistics.api.services.FileUploadService import FileUploadService

log = logging.getLogger(__name__)
ns = api.namespace('file_upload', description='Controller defining CRUD operations for File Upload')


@ns.route('/')
class FileUpload(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jsonEncoder = JSONEncoder()
        self.fileUploadService = FileUploadService()

    @api.expect(upload_parser)
    @api.response(201, 'File successfully uploaded')
    @api.response(400, 'Bad data request')
    def post(self):

        if not request.files['file'] or int(request.headers['Content-Length']) <= 0:
            err_msg = 'No data file provided'
            log.error(err_msg)
            return err_msg, 400

        try:
            self.fileUploadService.process(request.files['file'])
        except Exception as err:
            err_msg = f'Problem processing spreadsheet file. Internal error: {err}'
            log.error(err_msg)
            return Response(self.jsonEncoder.encode(err_msg), mimetype='application/json', status=500)

        return Response(self.jsonEncoder.encode(None), mimetype='application/json', status=201)