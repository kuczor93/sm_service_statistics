import logging.config

from flask import Flask, Blueprint
from flask_pymongo import PyMongo
from flask_cors import CORS

import service_statistics.api.controllers.FileUploadController
import service_statistics.api.controllers.FixturesController
import service_statistics.api.controllers.SeasonsController
from service_statistics import settings
from service_statistics.api.restplus import api
from os import path

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
log_path = path.join(path.dirname(path.abspath(__file__)), 'logging.conf')
logging.config.fileConfig(log_path)
log = logging.getLogger(__name__)

def configure_app(flask_app: Flask):
    flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP

    flask_app.config['MONGO_DBNAME'] = settings.MONGODB_DBNAME
    flask_app.config['MONGO_URI'] = settings.MONGODB_URI


def initialize_app(flask_app: Flask):

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(service_statistics.api.controllers.SeasonsController.ns)
    api.add_namespace(service_statistics.api.controllers.FixturesController.ns)
    api.add_namespace(service_statistics.api.controllers.FileUploadController.ns)
    flask_app.register_blueprint(blueprint)

configure_app(app)
db = PyMongo(app)

if __name__ == '__main__':
    initialize_app(app)
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run()